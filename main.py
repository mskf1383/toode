import feedparser
import chevron

with open("feeds.txt", "r") as f:
    feeds = f.readlines()

with open("top.html", "r") as f:
    print(chevron.render(f))

for feed_url in feeds:
    feed = feedparser.parse(feed_url)

    for entry in feed.entries:
        with open("content.html", "r") as f:
            print(chevron.render(f, {
                "title": entry.title,
                "link": entry.links[0]['href'],
                "description": entry.description,
                "author": entry.author,
                "publisher": feed.feed.title,
                "publisher_link": feed.feed.link,
            }))
            # print(entry)

with open("bottom.html", "r") as f:
    print(chevron.render(f))
